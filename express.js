'use strict';

// Common Modules...
const _ = require('lodash');
const Path = require('path');
const Express = require('express');
const BodyParser = require('body-parser');
const CookieParser = require('cookie-parser');
const Cors = require('cors');
const MethodOverride = require('method-override');

// Beginning of Logic!
module.exports = (app) => {
  app.use(Express.static(Path.join(__dirname, '/public')));

  // Express.js 3rd-Party Middleware
  app.use(BodyParser.json({
    limit: '10mb',
  }));
  app.use(BodyParser.urlencoded({
    limit: '10mb',
    extended: true,
  }));

  app.use(MethodOverride());
  app.use(CookieParser());

  // API 서버: 외부 IP에서 Ajax 요청 허용
  app.use(Cors({
    origin: '*',
    credentials: false,
  }));

  // API 에러 출력 미들웨어
  app.use((err, req, res, next) => {
    log.error('API Error :', err);
  });
};