'use strict';

// Common Modules...
const Express = require('express');
const http = require('http');
const domain = require('domain');

// Own Modules...
const db = require('./libs/db.js');
const Routes = require('./routes');
const Logger = require('./libs/logger');

// express setting
const app = Express();
const mExpress = require('./express');
mExpress(app);

// custom setting
process.env.TZ = 'Asia/Seoul';
global.log = Logger('./logs/debug.log');

// run domain
const serverDomain = domain.create();
serverDomain.run(() => {
  // 서버 실행

  const httpServer = http.createServer(app);
  httpServer.listen(8080);

  // DB 연결
  db.connect();

  // KeywordQueue.deleteQueue();

  /*
  KeywordQueue.initiate()
  .then(()=>{
    MediumScrapper.processing();
  });
  */

  app.use('/api', Routes.routes());
});

serverDomain.on('error', (err) => {
  log.error('Domain Error :', err);
});