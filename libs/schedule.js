const schedule = require('node-schedule');
const fs = require('fs');
const async = require("async");

exports.periodic = (time, callback) => {
  const hour = time.hour;
  const minute = time.minute;

  const j = schedule.scheduleJob(`0 ${minute} ${hour} * * 0-5`, callback);
};