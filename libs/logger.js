const winston = require('winston');
const moment = require('moment');

module.exports = (filename) => {
  console.log(filename);
  
  const logger = new winston.Logger({
    transports: [
      new winston.transports.Console({
        level      : 'info',
        colorize: true,
        prettyPrint: true,
        timestamp : function(){
          return moment().format('YYYY-MM-DD HH:mm:ss.SSS');
        }
      }),
      new winston.transports.File({
        level      : 'debug',
        json       : false,
        filename   : filename,
        colorize: false,
        prettyPrint: true,
        timestamp : function(){
          return moment().format('YYYY-MM-DD HH:mm:ss.SSS');
        }
      })
    ]
  });

  //Handling Uncaught Exceptions

  /*
  logger.handleExceptions(new winston.transports.File({
      filename: filename+'_exception',
      handleExceptions: true,
      humanReadableUnhandledException: true,
      prettyPrint: true,
      json : false
  }));
  */
  
  logger.setLevels(winston.config.syslog.levels);
  logger.exitOnError = false;
  return logger;
};