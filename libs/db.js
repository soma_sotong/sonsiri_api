// binding mongoose module
const mongoose = require('mongoose');

// exports connect function to app.js
exports.connect = () => {
  let intendedCloseFlag = false;
  let isConnected = false;

  // get the database connection pool
  /*
  mongoose.connect('localhost:27017/sotong', {
    user: 'abc',
    pass: 'abc',
  });
  */
  mongoose.connect('localhost:27017/sotong');

  // Use native promises
  mongoose.Promise = global.Promise;

  // DB Connection Events
  // Succeed to connect database
  mongoose.connection.on('connected', () => {
    isConnected = true;
    log.notice('Succeed to get connection pool in mongoose');
  });

  // Failed to connect database
  mongoose.connection.on('error', (err) => {
    log.error('Failed to get connection in mongoose, err is ', err);
  });

  // When the connection has disconnected
  mongoose.connection.on('disconnected', () => {
    isConnected = false;
    log.notice('Database connection has disconnected.');
  });

  //If the Node.js process is going down, close database connection pool
  process.on('SIGINT', () => {
    intendedCloseFlag = true;
    mongoose.connection.close(() => {
      log.notice('Application process is going down, disconnect database connection...');
      process.exit(0);
    });
  });
};