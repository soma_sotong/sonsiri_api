/**
 * Created by bowbowbow on 2017. 2
 */

// Common Modules...
const Express = require('express');

exports.routes = (app) => {
  const Router = new Express.Router({
    mergeParams: true,
  });

  Router.route('/ping')
  .get((req, res) => {
    res.end('pong');
  });

  Router.route('/dashboard')
  .get((req, res) => {
    /*
    const data = {
      keywordCount: 65232,
      queueCount: 70927,
      processCount: 1,
      recentKeywords: ['astrologie', 'selbstbewusstsein', 'inneres-wachstum', 'la-marato'],
      dailyKeywordCount: [3000, 27000, 4200, 38000, 2500, 3200, 7600, 4500, 2000, 1580],
    };

    KeywordQueue.getQueueSize()
    .then((queueSize) => {
      data.queueCount = queueSize;
      return KeywordController.getKeywordCount();
    }).then((keywordCount) => {
      data.keywordCount = keywordCount;
      return KeywordController.getRecentkeyword(100);
    }).then((keywords) => {
      data.recentKeywords = keywords.map((keyword) => {
        return keyword.keyword;
      });

      res.end(JSON.stringify(data));
    }).catch((err) => {
      console.log(err);
    });
    */
  });
  return Router;
};