const mongoose = require('mongoose'), Schema = mongoose.Schema;

const keywordSchema = new Schema({
  keyword : {type: String, default: "EMPTY", index: true},
  related : [{type: mongoose.Schema.Types.ObjectId, ref : 'keywords'}],
  createdTime: {type: Date, default: Date.now},  // 생성 시간
},{
    versionKey: false // You should be aware of the outcome after set to false
});

//exports model for task-controller
module.exports = mongoose.model('keywords', keywordSchema);