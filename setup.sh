#!/bin/bash
printf 'check Node.JS installed'
if node; then
    printf 'Node.JS installed'
else
    printf 'install Node.JS'
    curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
    sudo apt-get install -y nodejs
    printf 'install build essential'
    sudo apt-get install -y build-essential
fi

if mongo; then
    printf 'MongoDB installed'
else
    printf 'install MongoDB'
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
    echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
    sudo apt-get update
    sudo apt-get install -y mongodb-org

printf 'intall npm modules'
npm preinstall
npm install