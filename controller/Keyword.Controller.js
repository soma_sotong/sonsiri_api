/**
 * @author Seungwon.Yoon <cslrn1581@gmail.com>
 * @since 2017.2
 */

'use strict';

// Common Modules...
const _ = require('lodash');
const Immutable = require('immutable');

// Own Modules...
const keywordm = require('../models/Keyword.Model');

// Beginning of Logic!
class KeywordController {
  constructor() {
  }

  /**
   * 키워드를 이름으로 가져온다.
   * @returns {Promise}
   */
  getKeyword(keyword) {
    return new Promise((resolve, reject) => {
      if (_.isString(keyword)) {
        keywordm.findOne({
          keyword,
        }).then((keyword) => {
          resolve(keyword);
        });
      } else {
        reject();
      }
    });
  }

  /**
   * 연관 키워드를 포함시켜서 키워드를 가져온다.
   * @param keyword {string}: 가져올 키워드
   * @returns {Promise}
   */
  getKeywordPopulate(keyword) {
    return new Promise((resolve, reject) => {
      if (_.isString(keyword)) {
        keywordm.findOne({
          keyword,
        }).populate('related').then((keyword) => {
          resolve(keyword);
        });
      } else {
        reject();
      }
    });
  }

  /**
   * 최근 limit 개의 키워드를 가져온다.
   * @param limit {number}: 가져올 키워드 개수
   * @returns {Promise}
   */
  getRecentkeyword(limit) {
    return new Promise((resolve, reject) => {
      if (_.isNumber(limit)) {
        keywordm.find({}).sort({createdTime: -1}).limit(limit).then((keyword) => {
          resolve(keyword);
        });
      } else {
        reject();
      }
    });
  }


  /**
   * 키워드를 아이디로 가지고 온다.
   * @param _id
   * @returns {Promise}
   */
  getKewordById(_id) {
    return new Promise((resolve, reject) => {
      if (_.isString(_id)) {
        keywordm.findOne({
          _id,
        }).then((keyword) => {
          resolve(keyword);
        });
      } else {
        reject();
      }
    });
  }

  /**
   *
   * @param args.keyword {string} : update 할 키워드
   * @param args.relatedIds {array} : 추가 할 연관 키워드 아이디
   * @returns {Promise}
   */
  addRelatedToKeyword(args) {
    const keyword = args.keyword;
    const relatedIds = args.relatedIds;

    return new Promise((resolve, reject) => {
      if (_.isString(keyword) && _.isArray(relatedIds)) {
        keywordm.findOne({
          keyword,
        }).then((keywordM) => {
          if (keywordM) {
            keywordM.related = relatedIds;

            keywordM.save()
            .then((keywordM) => {
              resolve(keywordM);
            });
          } else {
            // 파라미터로 들어온 키워드가 디비에 존재하지 않는 상황
            this.create(keyword)
            .then((keywordM) => {
              keywordM.related = relatedIds;
              keywordM.save()
              .then((keywordM) => {
                resolve(keywordM);
              })
            });
          }
        });
      } else {
        reject(':: addRelatedToKeyword :: parameter error');
      }
    });
  }

  /**
   * 중복된 키워드가 존재하지 않을 때 키워드를 추가한다.
   * @returns {Promise}
   */
  create(keyword) {
    return new Promise((resolve, reject) => {
      if (keyword) {
        this.getKeyword(keyword)
        .then((keywordM) => {
          if (!keywordM) {
            // 이미 같은 키워드가 디비에 존재하지 않는다면
            keywordm.create({
              keyword,
            }).then((keywordM) => {
              resolve(keywordM);
            }, (err) => {
              reject(err);
            });
          } else {
            resolve(keyword);
          }
        });
      } else {
        reject();
      }
    });
  }

  /**
   * 있으면 가져오고 없으면 만들어서 가져간다.
   * @param keyword
   * @returns {Promise}
   */
  getKeywordForce(keyword) {
    return new Promise((resolve, reject) => {
      if (_.isString(keyword)) {
        keywordm.findOne({
          keyword,
        }).then((keywordM) => {
          if (keywordM) {
            resolve({
              keyword: keywordM,
              exist: true,
            });
          } else {
            this.create(keyword)
            .then((keywordM) => {
              resolve({
                keyword: keywordM,
                exist: false,
              });
            })
          }
        });
      } else {
        reject();
      }
    });
  }

  /**
   * 현재 보유한 키워드의 개수를 반환한다.
   * @returns {Promise}
   */
  getKeywordCount(){
    return new Promise((resolve, reject) => {
      keywordm.find({}).count()
      .then((count) => {
        resolve(count);
      });
    });
  }
}

// Module Exports...
module.exports = exports = new KeywordController();
exports.default = KeywordController;

